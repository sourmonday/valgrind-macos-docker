
Following Gungorbudak's guide for setting up memory test due to MacOS High Sierra+ incompatibility with valgrind

https://www.gungorbudak.com/blog/2018/06/13/memory-leak-testing-with-valgrind-on-macos-using-docker-containers/

To stand up the docker container:
```bash
docker build -t memory-test:0.1 memory-test/
```

command being:
```bash
docker run -ti -v $PWD:/test memory-test:0.1 bash -c "cd /test/; g++ -o main main.cpp && valgrind --leak-check=full 
./main"
```

working command with cmake:
```bash
docker run -ti -v $PWD:/test memory-test:0.1 bash -c "mkdir -p /memory-test; cp -r /test/* /memory-test; \
cd /memory-test; cmake . && make && valgrind --leak-check=full ./valgrind_test"
```