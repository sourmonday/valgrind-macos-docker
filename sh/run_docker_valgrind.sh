#!/usr/bin/env bash

docker run -i -v $PWD:/test memory-test:0.1 bash -c "mkdir -p /memory-test; cp -r /test/* /memory-test; \
cd /memory-test; cmake . && make && valgrind --leak-check=full --leak-resolution=med --show-leak-kinds=all \
--track-origins=yes --vgdb=no --tool=memcheck --gen-suppressions=all ./valgrind_test"
